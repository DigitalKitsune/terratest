require "love.timer"
local Generator = Class:extend()

function Generator:new(meta, data)
  local t = love.timer.getTime()
  local map, sx, sy = data.map, data.size.x, data.size.y
  print(string.format("generating %dx%d map",sx,sy))
  for y = 0, sy-1 do
    for x = 0, sx-1 do
      if (y < sy/2) or (math.random() > 0.7) then
        map[y*sx + x] = 1
      else
        map[y*sx + x] = 2
      end
    end
    --print(string.format("generated line %d", y))
  end
  print(string.format("generated in %f seconds", love.timer.getTime() - t))
end

Generator:registerAs "World.Generator"