require "Native.Memory"

local Data = Class:extend()

function Data:new(size, pointer)
  assert(type(size) == "table", "Provide a size to constructor")
  self.size = size
  if pointer then
    self.mem = Native.Memory(pointer,size)
  else
    self.mem = Native.Memory()
    self.mem:allocate(self.size.x * self.size.y)
  end
  self.map = self.mem.pointer
end

Data:registerAs "World.Data"

--[=[
local mem = Native.Memory()
mem:allocate(4096)
local threadCode =
  [[
-- Receive values sent via thread:start
local a, b = ...
require 'Class'
require 'Native.Memory'
mem = Native.Memory()
mem:fromNumPair(a, b)
p = mem:getPointer()
p[0] = 1;
p[1] = 10
  ]]
local thread = love.thread.newThread(threadCode)
thread:start(mem:toNumPair())
love.timer.sleep(1)
local p = mem:getPointer()
print(p[0], p[1])
]=]
