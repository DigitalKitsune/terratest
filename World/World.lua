require "Util.Config"
require "World.Data"
require "World.Generator"

local sizes = {
  tiny = {x = 2048, y = 1024},
  small = {x = 4096, y = 2048},
  medium = {x = 8192, y = 4096},
  large = {x = 16384, y = 8192}
}

local params = {
  name = {"string"},
  size = {"string", {"tiny","small","medium","large"}},
  time = {"number"},
}

local world = Class:extend()

function world:new(a, b)
  if type(a) == "table" then
    self.meta = Util.Config(a)
    if b then
      self.data = World.Data(sizes[self.meta.size], b)
    else
      self.data = World.Data(sizes[self.meta.size])
    end
  else
    self:load(a)
  end
end

function world:load(name)
  self.meta = Util.Config(string.format("Worlds/%s/world.meta",name))
  self.data = World.Data(sizes[self.meta.size])
end

function world:generate()
  self.generator = World.Generator(self.meta, self.data)
end

function world:connect(a)

end

world:registerAs "World.World"
