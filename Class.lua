--
-- classic
--
-- Copyright (c) 2014, rxi
--
-- Modified by DigitalKitsune in 2019
--
-- This module is free software; you can redistribute it and/or modify it under
-- the terms of the MIT license. See LICENSE for details.
--

local Class = {}
Class.__index = Class

function Class.new()
end

function Class:extend()
  local cls = {}
  for k, v in pairs(self) do
    if k:find("__") == 1 then
      cls[k] = v
    end
  end
  cls.__index = cls
  cls.super = self
  setmetatable(cls, self)
  return cls
end

function Class:implement(...)
  for _, cls in pairs({...}) do
    for k, v in pairs(cls) do
      if self[k] == nil and type(v) == "function" then
        self[k] = v
      end
    end
  end
end

function Class:is(T)
  local mt = getmetatable(self)
  while mt do
    if mt == T then
      return true
    end
    mt = getmetatable(mt)
  end
  return false
end

function Class.__tostring()
  return "Class"
end

function Class:__call(...)
  local obj = setmetatable({}, self)
  obj:new(...)
  return obj
end

-- Globally registers class
function Class:registerAs(hierarchy)
  local t = _G
  local t_last
  local i_last
  for i in string.gmatch(hierarchy, "([^.]+)") do
    t_last = t
    if type(t[i]) ~= "table" then
      t[i] = {}
    end
    t = t[i]
    i_last = i
  end

  local temp = getmetatable(self)

  if not temp then
    temp = {};
    setmetatable(self, temp);
  end

  function temp.__tostring()
    return hierarchy
  end

  t_last[i_last] = self
  print("registered " .. hierarchy)
end

Class:registerAs("Class")
