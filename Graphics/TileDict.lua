require "Util.Config"
require "Graphics.Tile"
require "Graphics.TextureAtlas"

local TileDict = Class:extend()

function TileDict:new()
  self.atlas = Graphics.TextureAtlas()
  local tiles = Util.Config("Content/Tile.list")
  for i, v in ipairs(tiles) do
    self[i] = v
    if v.texture then
      _, self[i].quad = self.atlas:add(v.texture)
    end
  end
end

TileDict:registerAs "Graphics.TileDict"