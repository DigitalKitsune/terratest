local TextureAtlas = Class:extend()

function TextureAtlas:new(row)
  if (row) then
    self.texture = love.graphics.newCanvas(row * 8, row * 8)
    self.row = row
  else
    self.texture = love.graphics.newCanvas(1024, 1024)
    self.row = 128
  end
  self.count = 0
end

function TextureAtlas:add(image)
  assert(image)
  if type(image) == "string" then
    image = love.graphics.newImage( image )
  end
  local row = self.row
  self.count = self.count + 1
  assert(self.count < row * row, "TextureAtlas overload!")
  local x = (self.count % row) * 8
  local y = math.floor(self.count / row) * 8
  love.graphics.setCanvas(self.texture)
  love.graphics.draw(image,x,y)
  love.graphics.setCanvas()
  return self.count, love.graphics.newQuad(x,y,8,8,row*8,row*8)
end


function TextureAtlas:get(id)
  assert(id > self.count, "id out of range")
  local row = self.row
  local x = (self.count % row) * 8
  local y = math.floor(self.count / row) * 8
  return love.graphics.newQuad(x,y,8,8,row*8,row*8)
end

TextureAtlas:registerAs "Graphics.TextureAtlas"