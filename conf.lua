function love.conf(t)
  t.identity = "TerraTest"
  t.window.title = "TerraTest"
  t.window.resizable = true -- Let the window be user-resizable (boolean)
  t.window.minwidth = 640 -- Minimum window width if the window is resizable (number)
  t.window.minheight = 480 -- Minimum window height if the window is resizable (number)
  t.window.fullscreen = false
end
