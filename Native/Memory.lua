local ffi = require("ffi")

ffi.cdef [[
void *mmap(void *addr, size_t length, int prot, int flags, int fd, long int offset);
int munmap(void *addr, size_t length);
]]

local Memory = Class:extend()

function Memory:new(pointer, size)
  if pointer then
    assert(size,"Provide a size!")
    if type(pointer) == "cdata" then
      self.pointer = ffi.cast("uint8_t*", pointer)
    else
      self:fromNumPair(pointer[1], pointer[2])
    end
    self.size = size
  end
end

function Memory:allocate(size)
  self.size = size
  local shift = ffi.cast("void *", 0x100000000)
  self.pointer = ffi.cast("uint8_t*", ffi.C.mmap(shift, size, 0x3, 0x21, -1, 0))
  assert(self.pointer ~= nil, "Memory allocation failed!")
end

function Memory:deallocate()
  ffi.C.munmap(self.pointer, self.size)
  self.size = 0
end

function Memory:fromNumPair(a, b)
  assert((type(a) == "number") and (type(b) == "number"), "Wrong operands!")
  local t = ffi.new [[union{uint8_t * p; uint32_t i[2];}]]
  t.i[0], t.i[1] = a, b
  self.pointer = t.p
end

function Memory:toNumPair()
  local t = ffi.new [[union{uint8_t * p; uint32_t i[2];}]]
  t.p = self.pointer
  return t.i[0], t.i[1]
end

function Memory:getPointer()
  return self.pointer
end

Memory:registerAs("Native.Memory")
