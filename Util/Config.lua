local Config = Class:extend()

function Config:new(a)
  if a then
    local t = type(a)
    if t == "string" then
      self:load(a)
    elseif t == "table" then
      for k, v in pairs(a) do
        self[k] = v
      end
    else
      assert(false, "Wrong parameter!")
    end
  end
end

function Config:save(name)
  if name then
    assert(love.filesystem.write(name, self:serialize()))
  elseif self.__name then
    assert(love.filesystem.write(self.__name, self:serialize()))
  else
    assert(false, "No filename provided")
  end
end

function Config:serialize()
  local s
  if type(self) == "table" then
    s = "{\n"
    for k, v in pairs(self) do
      if k:sub(1, 2) ~= "__" then
        if type(k) ~= "number" then
          k = '"' .. k .. '"'
        end
        s = s .. "[" .. k .. "]=" .. Config.serialize(v) .. ","
      end
    end
    return s .. "}"
  else
    s = string.format("%q", self)
  end
  return s
end

function Config:load(s)
  local c, z
  if not string.find(s, "{") then
    c, z = love.filesystem.read(s)
    assert(c, z)
    self.__name = s
  else
    c = s
  end

  local f, err = load("return " .. c, "=deserialize", "t", {})
  assert(f, err)
  local co = coroutine.create(f)
  debug.sethook(co, error, "", 1000000)
  local _, t = coroutine.resume(co)
  debug.sethook(co)
  assert(type(t) == "table", "Wrong file/format!")
  for k, v in pairs(t) do
    if type(k) ~= "string" or k:sub(1, 2) ~= "__" then
      self[k] = v
    end
  end
end

function Config:validate(val)
  for k, v in pairs(self) do
    if k:sub(1, 2) ~= "__" then
      if val[k] then
        if type(v) == val[k][1] then
          if val[k][2] then
            local suc = false
            for _, d in ipairs(val[k][2]) do
              suc = succ or v == d
            end
            assert(suc)
          end
        else
          error('type mismatch')
        end
      else
        self[k] = nil
      end
    end
  end
end

Config:registerAs "Util.Config"
