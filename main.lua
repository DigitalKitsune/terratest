function love.load()
  local _print = _G.print
  _G.print = function(...)
    _print("Client:", ...)
  end
  require "Class"
  require "UI.Logo"
  love.graphics.setBackgroundColor(0.349, 0.615, 0.862)
end

local loading

local function load()
  love.graphics.setDefaultFilter("nearest", "nearest")
  require "Util.Config"
  require "UI.Main"
  require "Engine.Server"
  require "Graphics.TileDict"
  require "UI.View"
  config = Util.Config("game.cfg")
  ui = UI.Main()
  server = Engine.Server()
  tiles = Graphics.TileDict()
  view = UI.View()
  love.update = function(dt)
    ui:update(dt)
    server:update()
  end
  love.draw = function()
    ui:draw()
  end
end

function love.update()
  if loading then
    load()
    love.timer.step()
  end
  loading = true
end

function love.draw()
  UI.Logo.draw()
end
