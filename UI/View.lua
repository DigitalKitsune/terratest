require "Graphics.TileDict"

local View = Class:extend()

function View:new()
  self.batch = love.graphics.newSpriteBatch(tiles.atlas.texture, 10000, "dynamic")
  self.position = {x = 0, y = 0}
end
--local oneshot = false
function View:update()
  local size = server.world.data.size
  local map = server.world.data.map
  local tw, th = ui.width / 16, ui.height / 16
  local xs = math.floor(size.x / 2 - tw / 2 + self.position.x / 16)
  local xd = self.position.x % 16
  local ys = math.floor(size.y / 2 - th / 2 + self.position.y / 16)
  local yd = self.position.y % 16
  if xs < 0 then
    xs = 0
    xd = 0
  end
  if (xs + tw) > (size.x - 1) then
    xs = size.x - tw - 1
    xd = 0
  end
  if ys < 0 then
    ys = 0
    yd = 0
  end
  if (ys + th) > (size.y - 1) then
    ys = size.y - th - 1
    yd = 0
  end
  self.batch:clear()
  for y = 0, th do
    local yt = y + ys
    for x = 0, tw do
      local xt = x + xs
      local tile = tiles[map[yt * size.x + xt]]
      if tile.quad then
        self.batch:add(tile.quad, x * 16 - xd, y * 16 - yd, 0, 2, 2, 0, 0, 0, 0)
      end
    end
  end
end

function View:draw()
  love.graphics.draw(self.batch)
 --, 400, 300)
end

View:registerAs "UI.View"
