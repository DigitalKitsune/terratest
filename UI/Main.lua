require "UI.Menu"
require "UI.Mouse"
--require()

local Main = Class:extend()

function Main:new()
  self.width, self.height = love.window.getMode()

  love.handlers.resize = function(w, h)
    print(w, h)
    self.width = w
    self.height = h
  end
  love.handlers.loadFinished = function()
    self.menu.mode = 2
    self.view = view
    self.view:update()
    self.state = "inGame"
  end
  love.handlers.keypressed = function()
    self.view.position.x = self.view.position.x + 5
  end
  self.state = "mainMenu"
  self.menu = UI.Menu(self)
  self.mouse = UI.Mouse()
end

function Main:update()
  self.mouse:update()
  if self.state == "mainMenu" then
    self.menu:update()
  elseif self.state == "inGame" then
    self.menu:update()
    self.view:update()
  end
end

function Main:draw()
  if self.state == "mainMenu" then
    self.menu:draw()
  elseif self.state == "inGame" then
    self.view:draw()
    self.menu:draw()
  end
end

Main:registerAs("UI.Main")
