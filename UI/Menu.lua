local Menu = Class:extend()

function Menu:new(parent)
  self.parent = parent
  self.normal = love.graphics.newFont(26)
  self.hiligh = love.graphics.newFont(30)
  self.mode = 1
  self.labels = {
    {
      {
        "Singleplayer",
        0,
        2
      },
      {"Multiplayer", 0, 3},
      {"Settings", 0, 4},
      {"Exit", 0, 1}
    },
    {
      {
        "New world",
        0,
        function()
          love.event.push("createWorld")
          self.scr = 5
        end
      },
      {"World 1", 0, 2},
      {"World 2", 0, 2},
      {"World 3", 0, 2},
      {"Exit", 0, 1}
    },
    {
      {"New server", 0, 3},
      {"Connect to server", 0, 3},
      {"Server 1", 0, 3},
      {"Server 2", 0, 1}
    },
    {
      {"Sound: On", 0, 4},
      {"Music: On", 0, 4},
      {"Fullscreen: Off", 0, 4},
      {"Exit", 0, 1}
    },
    {
      {"Loading: 0%", 0, 5}
    }
  }
  self.scr = 1
  self.hilighted = 0
end

function Menu:update()
  if self.mode == 1 then
    local num = #self.labels[self.scr]
    local base = self.parent.height / 2 - 40 * num - 40
    local mouse = self.parent.mouse
    local hilighted
    for i, v in ipairs(self.labels[self.scr]) do
      v[2] = base + i * 80
      local dif = math.abs(mouse.y - v[2])
      if (dif < 25) and not hilighted then
        hilighted = i
      end
    end
    self.hilighted = hilighted
    if hilighted and mouse.pressed[1] == 2 then
      mouse.pressed[1] = 1
      local t = self.labels[self.scr][hilighted][3]
      if type(t) == "function" then
        t()
      else
        self.scr = self.labels[self.scr][hilighted][3]
      end
    end
  end
end

function Menu:draw()
  if self.mode == 1 then
    love.graphics.setFont(self.normal)
    for i, v in ipairs(self.labels[self.scr]) do
      local shift
      if i == self.hilighted then
        love.graphics.setFont(self.hiligh)
        shift = 15
      else
        love.graphics.setFont(self.normal)
        shift = 13
      end
      --print(v[1], v[2])
      love.graphics.printf(v[1], 0, v[2] - shift, self.parent.width, "center")
    end
  end
end

Menu:registerAs "UI.Menu"
