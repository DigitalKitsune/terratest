local Logo = Class:extend()

function Logo.draw()
  local w, h = love.window.getMode()
  love.graphics.origin()
  love.graphics.setColor(255, 255, 255)
  love.graphics.circle( "line", w/2, h/2, 100, 6 )
end

Logo:registerAs "UI.Logo"