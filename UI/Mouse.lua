local Mouse = Class:extend()

function Mouse:new()
  self.x, self.y = love.mouse.getPosition()
  self.pressed = {false, false, false}
  love.handlers.mousepressed = function(a,b,c)
    self.x, self.y = a, b
    self.pressed[c] = 2
  end
  love.handlers.mousereleased = function(a,b,c)
    self.x, self.y = a, b
    self.pressed[c] = false
  end
end

function Mouse:update()
  self.x, self.y = love.mouse.getPosition()
end

Mouse:registerAs "UI.Mouse"