local _print = _G.print
_G.print = function(...)
  _print("Server:", ...)
end

require "Class"
require "World.World"
require "World.Generator"
print("server started")
local world = World.World(...)
world:generate()
love.thread.getChannel("serverEvent"):push("loadFinished")
