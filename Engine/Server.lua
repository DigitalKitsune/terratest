require "World.World"

local Server = Class:extend()

function Server:new()
  self.event = love.thread.getChannel("serverEvent")
  love.handlers.loadWorld = function(world)
    self.world = World.World()
  end
  love.handlers.createWorld = function(world)
    print("called createWorld")
    self.world = World.World({size = "tiny"})
    self.thread = love.thread.newThread("Engine/ServerThread.lua")
    self.thread:start(self.world.meta, {self.world.data.mem:toNumPair()})
  end
  love.handlers.connectTo = function(address)
    self.world = World.World()
  end
  love.handlers.exitWorld = function()
  end
end

function Server:update()
  local event = self.event:pop()
  if event then
    print(event)
    love.event.push(event)
  end
end

Server:registerAs "Engine.Server"
